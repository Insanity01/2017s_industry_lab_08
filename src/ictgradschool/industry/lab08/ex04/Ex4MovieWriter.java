package ictgradschool.industry.lab08.ex04;
import java.io.*;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        File myFile = new File(fileName);

        try (PrintWriter writer = new PrintWriter(new FileWriter(myFile))) {

            writer.println(films.length);

            for (int i = 0; i < films.length; i++) {

                writer.println(films[i].getName() + "," + films[i].getYear() + "," + films[i].getLengthInMinutes() + "," + films[i].getDirector());
            }

            System.out.println("Success!");

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        // TODO Implement this with a PrintWriter

    }

    public static void main(String[] args) {

        new Ex4MovieWriter().start();
    }

}
