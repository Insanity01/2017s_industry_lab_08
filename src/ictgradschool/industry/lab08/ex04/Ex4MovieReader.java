package ictgradschool.industry.lab08.ex04;

import java.io.*;
import java.util.Scanner;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        Movie[] filmsObject = null;

        File myFile = new File(fileName);

        try (Scanner scanner = new Scanner(myFile)) {
            filmsObject = new Movie[Integer.parseInt(scanner.next())];
            scanner.useDelimiter(",|\\r\\n");

            for (int i = 0 ; i < filmsObject.length; i++) {
                String name = scanner.next();
                int year = Integer.parseInt(scanner.next());
                int length = Integer.parseInt(scanner.next());
                String director = scanner.next();

                filmsObject[i] = new Movie(name, year, length, director);
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        // TODO Implement this with a Scanner

        return filmsObject;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
