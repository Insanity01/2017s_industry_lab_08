package ictgradschool.industry.lab08.ex01;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        FileReader fr = null;

        try {

            fr = new FileReader("input2.txt");

            int i = fr.read();

            while (i != -1) {

                if (i == 69 || i == 101) {
                    numE++;
                }

                i = fr.read();

                total++;
            }

            fr.close();

            System.out.println("The number of characters is " + total);
            System.out.println("The number of E or e characters is " + numE);

        } catch (IOException e) {

            System.out.println("IO Error");

        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        BufferedReader br = null;

        try {

            total = 0;
            numE = 0;

            br = new BufferedReader(new FileReader("input2.txt"));

            String sentence = null;

            while ((sentence = br.readLine()) != null) {

                for (int i = 0; i < sentence.length(); i++) {

                    if (sentence.charAt(i) == 69 || sentence.charAt(i) == 101) {
                        numE++;
                    }

                    total++;
                }

            }

            System.out.println("Total Count is " + total);
            System.out.println("E count is " + numE);

        } catch (IOException e) {
            System.out.println("Input Output Error");
        }


        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
