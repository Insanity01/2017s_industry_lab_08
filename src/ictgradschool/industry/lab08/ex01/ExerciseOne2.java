package ictgradschool.industry.lab08.ex01;
import java.io.*;


public class ExerciseOne2 {

    private void fileReaderEx01() {
        int num = 0;
        FileReader fR = null;
        try {
            fR = new FileReader("input1.txt");
            num = fR.read();
            System.out.println(num);
            System.out.println(fR.read());
            System.out.println(fR.read());
            System.out.println(fR.read());
            System.out.println(fR.read());
            fR.close();
        } catch (IOException e) {
            System.out.println("IO problem");
        }
    }

    public static void main(String[] args) {

        new ExerciseOne2().fileReaderEx01();
    }

}
