package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.

        System.out.println("Hi, please enter the name of the file.");
        String fileName = Keyboard.readInput();

        File myFile = new File(fileName);

        try (BufferedReader a = new BufferedReader(new FileReader(myFile))) {

            String text = null;

            while ((text = a.readLine()) != null) {
                System.out.println(text);
            }

        }

        catch (IOException e) {
            System.out.println("Error!");
        }


    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
