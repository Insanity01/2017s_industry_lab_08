package ictgradschool.industry.lab08.ex02;

import java.io.*;
import java.util.Scanner;

import ictgradschool.Keyboard;

public class MyScanner {

    public void start() {

        System.out.println("Enter Name of File");
        String fileName = Keyboard.readInput();
        File myFile = new File(fileName);

        try (Scanner newScanner = new Scanner(myFile)) {

            while (newScanner.hasNext()) {
                System.out.println(newScanner.nextLine());
            }
        } catch (IOException e) {
            System.out.println("Error");
        }


        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
