package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * A simple program which should allow the user to type any number of text lines. The program will then
 * write them out to a file.
 */
public class MyWriter {

    public void start() {

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // TODO Open a file for writing, using a PrintWriter.

        File myfile = new File(fileName);


        try (PrintWriter print = new PrintWriter(new FileWriter(myfile))) {

            while (true) {

                System.out.print("Type a line of text, or just press ENTER to quit: ");
                String text = Keyboard.readInput();
                print.println(text);

                if (text.isEmpty()) {
                    break;
                }

                // TODO Write the user's line of text to a file.
            }
        } catch (IOException e) {

            System.out.println("Input Error!");

        }

        System.out.println("Done!");

    }

    public static void main(String[] args) {

        new MyWriter().start();

    }
}
